import re
from requests_html import HTMLSession


def number_cleanup(phone_number):
    try:
        clean_phone_number = re.sub(r'\%20', '', phone_number)
        clean_phone_number = re.sub(r'[^\+\d]', '', clean_phone_number)
        return clean_phone_number
    except:
        return ''


def slash_join(*args):
    return "/".join(arg.strip("/") for arg in args)


def is_different_response(response1, response2):
    return response1.url.rstrip("/") != response2.url.rstrip("/")


def rendered_website_status_code(url):
    session = HTMLSession()
    response = session.get(url)
    response.html.render()
    return response.status_code


def rendered_website_html(url):
    session = HTMLSession()
    response = session.get(url)
    response.html.render(sleep=1)
    return response.html
