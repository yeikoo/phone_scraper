import csv
import time

from scrape import get_contact_subpage, number_cleanup, scrape_contact_number

start_time = int(round(time.time()))

with open('sample_data_js.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    next(reader, None)

    correct_subpage_count = 0
    correct_phone_count = 0
    incorrect_subpages = []
    incorrect_phones = []

    for i, row in enumerate(reader):
        test_start_time = int(round(time.time()))
        url = row[0]
        subpage = get_contact_subpage(url)
        if row[1] == subpage:
            correct_subpage_count += 1
        else:
            incorrect_subpages.append(subpage)

        number = number_cleanup(scrape_contact_number(subpage))
        if number_cleanup(row[2]) == number:
            correct_phone_count += 1
        else:
            incorrect_phones.append(number)

        if number != number_cleanup(row[2]):

            print(f'{i}:')
            print("_" * 75)
            print(f'request url page:\t{row[0]}')
            print(f'contact subpage:\t{subpage}')
            print(f'proper subpage:\t\t{row[1]}')
            print(f'contact number:\t\t{number}')
            print(f'proper number:\t\t{number_cleanup(row[2])}')
            print("_" * 75)
        else:
            print(f'test passed for {i}:\t{row[0]}')
            print('+' * 75)

        single_test_duration = int(round(time.time())) - test_start_time
        print(f'this test took: {single_test_duration} s')

duration = int(round(time.time())) - start_time

print("#" * 75)
print(f'incorrect phones list:\n{incorrect_phones}')
print("#" * 75)
print(f'{i+1} tests completed in: {duration} seconds')
print(f'correct_phones:\t{correct_phone_count}')
print(f'out of:\t\t{i+1}')
