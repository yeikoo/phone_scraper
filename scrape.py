import re
import requests
import sys

from bs4 import BeautifulSoup
from utils import is_different_response, number_cleanup, slash_join,\
    rendered_website_status_code, rendered_website_html

INTERNATIONAL_NUMBER_REGEX = r'(?:00|\+)(?:48|49)[\(\)\s\-\d]{6,20}'
LOCAL_NUMBER_REGEX = r'tel[\(\)\s\-\d\.\:\/]{6,20}'


def get_contact_subpage(url: str, render=False) -> str:

    if render is False:
        response = requests.get(url)
        url = response.url
        # check for most common url patterns to find contact information
        for subpage_candidate in ['/kontakt', '/contact', '/contact-us']:
            candidate_response = requests.get(slash_join(url, subpage_candidate))
            if candidate_response.status_code == 200 and is_different_response(response, candidate_response):
                return slash_join(url, subpage_candidate)

    #  check if most common url patterns will render out a website with status_code 200
    if render is True:
        response = rendered_website_html(url)
        for subpage_candidate in ['/kontakt', '/contact', '/contact-us']:
            candidate_response = rendered_website_status_code(slash_join(url, subpage_candidate))
            if candidate_response == 200:
                return slash_join(url, subpage_candidate)

    # check for url links by keywords
    url_content = response.text
    soup = BeautifulSoup(url_content, 'html.parser')
    contact_checking_regex = re.compile(r'kontakt|contact')
    contact_links = soup.find_all('a', href=contact_checking_regex)

    # sometimes links are in form /contact and somtimes full url is provided
    if not contact_links:
        return url
    elif contact_links[0].attrs.get('href')[0:4] != 'http':
        return slash_join(url, contact_links[0].attrs.get('href'))
    else:
        return contact_links[0].attrs.get('href')


def get_number(url: str, render=False) -> str:

    if render is True:
        response = rendered_website_html(url)
    else:
        response = requests.get(url)

    url_content = response.text
    soup = BeautifulSoup(url_content, 'html.parser')

    # in most cases the best way to find a contact number is to look for tag "a" with href starting with "tel"
    tel_reg = re.compile(r'tel:')
    phone_numbers_in_links = soup.find_all('a', href=tel_reg)

    for phone_number_in_links in phone_numbers_in_links:
        phone_number_in_links_href = phone_number_in_links['href']
        # I decided to go for international numbers in the first place
        phone_number_in_links = re.findall(INTERNATIONAL_NUMBER_REGEX, phone_number_in_links_href)
        if phone_number_in_links:
            return phone_number_in_links[0]

    # if there were no links with phone number I searched whole html for phone numbers
    international_phone_numbers_in_html = re.findall(INTERNATIONAL_NUMBER_REGEX, url_content)
    if international_phone_numbers_in_html:
        return international_phone_numbers_in_html[0]

    # if international number was not present, local number to be retrieved as a last option
    local_phone_numbers_in_html = re.findall(LOCAL_NUMBER_REGEX, url_content, flags=re.IGNORECASE)
    if local_phone_numbers_in_html:
        return local_phone_numbers_in_html[0]


def scrape_contact_number(url):
    contact_subpage_url = get_contact_subpage(url)
    phone_number = get_number(contact_subpage_url)
    if phone_number is None:
        contact_subpage_url = get_contact_subpage(url, render=True)
        phone_number = get_number(contact_subpage_url, render=True)

    return number_cleanup(phone_number)


if __name__ == '__main__':
    url = sys.argv[1]
    print(scrape_contact_number(url))
